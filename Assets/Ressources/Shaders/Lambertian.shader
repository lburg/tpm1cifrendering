﻿Shader "Unlit/Lambertian"
{
    Properties
    {
        _Color ("Color", Color) = (0,0,0,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            // TODO : implémenter le modèle Lambertien 
            // https://en.wikipedia.org/wiki/Lambertian_reflectance

            // " The reflection is calculated by taking the dot product of the surface's normal vector, N ,
            // and a normalized light-direction vector, L, pointing from the surface to the light source.
            // This number is then multiplied by the color of the surface and the intensity of the light hitting the surface. "

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float3 normal : NORMAL;
            };

            uniform float4 _Color;

            v2f vert (appdata v)
            {
                v2f o;
                // TODO
                /* Line to replace */ o.vertex = v.vertex;
                /* Line to replace */ o.normal = v.normal;
                return o; 
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = fixed4 (0,0,0,1);
                // TODO
                return col;
            }
            ENDCG
        }
    }
}
